const assert = require('assert')
const { checkEnableTime } = require("../JobPosting")

describe('JobPosting', function () {
  describe('checkEnableTime()', function () {
    it('should return true when เวลาสมัครอยู่ระหว่าง จุดเริ่มต้นและสิ้นสุด', function () {
      // Arrange
      const startTime = new Date(2021, 1, 31)
      const endTime = new Date(2021, 2, 5)
      const today = new Date(2021, 2, 3)
      const expectedResult = true
      // Act
      const actualResult = checkEnableTime(startTime, endTime, today)
      // Assert
      assert.strictEqual(actualResult, expectedResult)
    })

    it('should return false when เวลาสมัครอยู่ก่อนเวลาเริ่มต้น', function () {
      // Arrange
      const startTime = new Date(2021, 1, 31)
      const endTime = new Date(2021, 2, 5)
      const today = new Date(2021, 1, 30)
      const expectedResult = false;
      // Act
      const actualResult = checkEnableTime(startTime, endTime, today)
      // Assert
      assert.strictEqual(actualResult, expectedResult)
    })

    it('should return false when เวลาสมัครอยู่หลังเวลาสิ้นสุดการสมัคร', function () {
      // Arrange
      const startTime = new Date(2021, 1, 31)
      const endTime = new Date(2021, 2, 5)
      const today = new Date(2021, 2, 6)
      const expectedResult = false;
      // Act
      const actualResult = checkEnableTime(startTime, endTime, today)
      // Assert
      assert.strictEqual(actualResult, expectedResult)
    })

    it('should return true when เวลาสมัครเท่ากับเวลาเริ่มต้น', function () {
      // Arrange
      const startTime = new Date(2021, 1, 31)
      const endTime = new Date(2021, 2, 5)
      const today = new Date(2021, 1, 31)
      const expectedResult = true;
      // Act
      const actualResult = checkEnableTime(startTime, endTime, today)
      // Assert
      assert.strictEqual(actualResult, expectedResult)
    })

    it('should return true when เวลาสมัครเท่ากับเวลาสิ้นสุดการสมัคร', function () {
      // Arrange
      const startTime = new Date(2021, 1, 31)
      const endTime = new Date(2021, 2, 5)
      const today = new Date(2021, 2, 5)
      const expectedResult = true;
      // Act
      const actualResult = checkEnableTime(startTime, endTime, today)
      // Assert
      assert.strictEqual(actualResult, expectedResult)
    })
  })
})
